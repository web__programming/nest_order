import { IsNotEmpty, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Min(40)
  price: number;
}
